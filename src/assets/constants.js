import { HiOutlineHashtag, HiOutlineHome, HiOutlinePhotograph, HiOutlineUserGroup } from 'react-icons/hi';

export const genres = [
  { title: 'Pop', value: 'Pop' },
  { title: 'Hip-Hop/Rap', value: 'Hip-Hop/Rap' },
  { title: 'Hip-Hop', value: 'Hip-Hop' },
  { title: 'Dance', value: 'Dance' },
  { title: 'Electronic', value: 'Electronic' },
  { title: 'Soul', value: 'R&B/Soul' },
  { title: 'Alternative', value: 'Alternative' },
  { title: 'Latin', value: 'LATIN' },
  { title: 'Rock', value: 'Rock' },
  { title: 'Indie', value: 'Indie Pop' },
  { title: 'Film', value: 'FILM_TV' },
  { title: 'Country', value: 'COUNTRY' },
  { title: 'Worldwide', value: 'WORLDWIDE' },
  { title: 'Reggae', value: 'REGGAE_DANCE_HALL' },
  { title: 'House', value: 'HOUSE' },
  { title: 'K-Pop', value: 'K_POP' },
];

export const links = [
  { name: 'Discover', to: '/', icon: HiOutlineHome },
  { name: 'Around You', to: '/around-you', icon: HiOutlinePhotograph },
  { name: 'Top Artists', to: '/top-artists', icon: HiOutlineUserGroup },
  { name: 'Top Charts', to: '/top-charts', icon: HiOutlineHashtag },
];
