import loader from './loader.svg';
import logo from './logo.svg';
import songImg from './songImg.jpg';

export {
  logo,
  loader,
  songImg,
};
