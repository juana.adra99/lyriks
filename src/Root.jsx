import { useSelector } from "react-redux";
import { Outlet, useLocation } from "react-router-dom";

import { Searchbar, Sidebar, MusicPlayer, TopPlay } from "./components";
import { useEffect } from "react";

const Root = () => {
  const { activeSong } = useSelector((state) => state.player);

  const { pathname } = useLocation();

  useEffect(() => {
    const div = document.getElementById("container");
    if (div) {
      div.scrollTo({ top: 0, behavior: "smooth" });
    }
  }, [pathname]);

  return (
    <div className="relative flex">
      <Sidebar />
      <div className="flex-1 flex flex-col bg-gradient-to-br from-black to-[#121286]">
        <Searchbar />

        <div
          id="container"
          className="px-6 h-[calc(100vh-72px)] overflow-y-scroll hide-scrollbar flex xl:flex-row flex-col"
        >
          <div className="flex-1 h-fit pb-20 xl:pb-40">
            <Outlet />
          </div>
          <div className="xl:sticky h-fit mb-32">
            <TopPlay />
          </div>
        </div>
      </div>

      {activeSong?.title && (
        <div className="absolute h-28 bottom-0 left-0 right-0 flex animate-slideup bg-gradient-to-br from-white/10 to-[#2a2a80] backdrop-blur-lg rounded-t-3xl z-10">
          <MusicPlayer />
        </div>
      )}
    </div>
  );
};

export default Root;
