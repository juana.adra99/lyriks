/* eslint-disable jsx-a11y/media-has-caption */
import React, { useRef, useEffect, useState } from "react";

const Player = ({
  activeSong,
  isPlaying,
  volume,
  seekTime,
  onEnded,
  onTimeUpdate,
  onLoadedData,
  repeat,
}) => {
  const [song, setSong] = useState({});

  useEffect(() => {
    setSong(activeSong);
  }, [activeSong]);

  const ref = useRef(null);

  if (ref.current) {
    if (isPlaying) {
      setTimeout(() => {
        ref.current.play();
      }, 10);
    } else {
      ref.current.pause();
    }
  }

  useEffect(() => {
    ref.current.volume = volume;
  }, [volume]);
  // updates audio element only on seekTime change (and not on each rerender):
  useEffect(() => {
    ref.current.currentTime = seekTime;
  }, [seekTime]);

  return (
    <audio
      src={
        song?.hub?.actions
          ? song?.hub?.actions[1]?.uri ||
            song?.hub?.options[0]?.actions[1]?.uri
          : song?.streaming?.preview
      }
      ref={ref}
      loop={repeat}
      onEnded={onEnded}
      onTimeUpdate={onTimeUpdate}
      onLoadedData={onLoadedData}
      muted={false}
    />
  );
};

export default React.memo(Player);
