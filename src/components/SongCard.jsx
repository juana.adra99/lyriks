import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";

import PlayPause from "./PlayPause";
import { playPause, setActiveSong } from "../redux/features/playerSlice";
import { songImg } from "../assets";

const SongCard = ({ songId, song, isPlaying, activeSong, data, i }) => {
  const dispatch = useDispatch();

  const handlePauseClick = () => {
    dispatch(playPause(false));
  };

  const handlePlayClick = () => {
    dispatch(setActiveSong({ song, data, i }));
    dispatch(playPause(true));
  };

  return (
    <div className="flex flex-col w-[250px] p-4 bg-white/5 bg-opacity-80 backdrop-blur-sm animate-slideup rounded-lg cursor-pointer">
      <div className="relative w-full group">
        <div
          className={`absolute inset-0 justify-center items-center flex bg-black bg-opacity-30 group-hover:flex ${
            activeSong?.title === song?.title
              ? "bg-black bg-opacity-70"
              : "md:hidden"
          }`}
        >
          <PlayPause
            isPlaying={isPlaying}
            activeSong={activeSong}
            song={song}
            handlePlay={handlePlayClick}
            handlePause={handlePauseClick}
          />
        </div>
        <img
          alt="song_img"
          src={song?.images?.coverArt || song?.images?.coverart || songImg}
        />
      </div>
      <div className="mt-4 flex flex-col">
        <p className="font-semibold text-lg text-white truncate">
          <Link to={`/songs/${songId}`}>{song?.title}</Link>
        </p>
        <p className="text-sm truncate text-gray-300 mt-1">
          <Link
            to={
              song?.artists
                ? `/artists/${song?.artists[0]?.adamid}`
                : "/top-artists"
            }
          >
            {song?.artist || song?.subtitle}
          </Link>
        </p>
      </div>
    </div>
  );
};

export default SongCard;
