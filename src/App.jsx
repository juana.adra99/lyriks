import { RouterProvider, createBrowserRouter } from "react-router-dom";

import Root from "./Root";
import {
  ArtistDetails,
  TopArtists,
  AroundYou,
  Discover,
  Search,
  SongDetails,
  TopCharts,
} from "./pages";

const App = () => {
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Root />,
      children: [
        {
          index: true,
          element: <Discover />,
        },
        {
          path: "top-artists",
          element: <TopArtists />,
        },
        {
          path: "top-charts",
          element: <TopCharts />,
        },
        {
          path: "around-you",
          element: <AroundYou />,
        },
        {
          path: "artists/:id",
          element: <ArtistDetails />,
        },
        {
          path: "songs/:songid",
          element: <SongDetails />,
        },
        {
          path: "search/:searchTerm",
          element: <Search />,
        },
      ],
    },
  ]);

  return <RouterProvider router={router} />;
};

export default App;
